package com.pegueroles.microhttpd;

import java.io.IOException;

import com.pegueroles.microhttpd.MicroHTTPd;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class MicroHTTPdMIDlet extends MIDlet {

	MicroHTTPd httpdServer = null; 
	Thread httpdThread = null;
	
	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		// TODO Auto-generated method stub
	}

	protected void pauseApp() {
		// TODO Auto-generated method stub
	}

	protected void startApp() throws MIDletStateChangeException {
		// TODO Auto-generated method stub
		int port = 80;
		try {
			port = Integer.parseInt(getAppProperty("PORT")) ;
		}catch ( Exception e ){
		}
		try {
			httpdServer = new MicroHTTPd(port);
			httpdThread = new Thread(httpdServer);
			httpdThread.start(); 
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
	}

}
