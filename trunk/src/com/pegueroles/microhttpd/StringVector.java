package com.pegueroles.microhttpd;
import java.util.Vector;


public class StringVector extends Vector {
    
    /** Creates a new instance of StringVector */
    public StringVector() {
        super();
    }
    
    public StringVector(String str, String delim){
        super();
        StringBuffer temp = new StringBuffer(500);
        char c;
        for (int i=0; i < str.length(); i++){
        	c = str.charAt(i);
            if ( delim.indexOf(c) < 0 ){
                temp.append(c);
            }
            else {
            	if (temp.length() > 0)
            		this.addElement((String)temp.toString());
                temp = new StringBuffer(500);
            }
        }
        this.addElement(temp.toString());
    }
    
    public String stringElementAt(int i ){
        return (String)elementAt(i);
    }
    
    public int intElementAt(int i){
        try{
            return Integer.parseInt(stringElementAt(i));
        }
        catch(Exception e){
            return 0;
        }
    }
    
    public long longElementAt(int i){
        try{
            return Long.parseLong(stringElementAt(i));
        }
        catch(Exception e){
            return 0;
        }
    }

    
}
