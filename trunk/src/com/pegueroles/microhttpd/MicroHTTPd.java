package com.pegueroles.microhttpd;
import java.util.*;

import java.io.IOException;

import javax.microedition.io.ServerSocketConnection;
import javax.microedition.io.SocketConnection;
import javax.microedition.io.Connector;

import com.pegueroles.microhttpd.Response;
import com.pegueroles.microhttpd.Request;


public class MicroHTTPd implements Runnable
{
	// ==================================================
	// API parts
	// ==================================================

	/**
	 * Override this to customize the server.<p>
	 *
	 * (By default, this delegates to serveFile() and allows directory listing.)
	 *
	 * @parm uri	Percent-decoded URI without parameters, for example "/index.cgi"
	 * @parm method	"GET", "POST" etc.
	 * @parm parms	Parsed, percent decoded parameters from URI and, in case of POST, data.
	 * @parm header	Header entries, percent decoded
	 * @return HTTP response, see class Response for details
	 */
	
	public Response serve( String uri, String method, Hashtable header, Hashtable parms )
	{

		StringVector sv = new StringVector(uri,"/\\");
		
		String urlStart = uri.replace('\\','/');
		String urlEnd ="";
		while(true){
			PageDispatcher page = findPageDispatcher(urlStart.toString());
			if (page!=null){
				return page.serve(urlEnd.toString(), method, header, parms);
			}
			int ind = urlStart.lastIndexOf('/');
			if (ind > 0){
				urlEnd = urlStart.substring(ind) + "/" + urlEnd;
				urlStart = urlStart.substring(0,ind -1);
			}else{
				break;
			}
		}

		PageDispatcher page = defaultPageDispatcher(uri);
		if (page!=null){
			return page.serve(uri, method, header, parms);
		}
		
		
		StringBuffer text = new StringBuffer();
		text.append("METHOD: " + method +"\n");
		text.append("URI: " + uri +"\n");
		text.append("\nHeaders: \n\n");
		Enumeration e = header.keys();
		while ( e.hasMoreElements())
		{
			String key = (String)e.nextElement();
			text.append("   "+key + "=" + (String)header.get( key )+"\n");
		}
		text.append("\nParams: \n\n");
		e = parms.keys();
		while ( e.hasMoreElements())
		{
			String key = (String)e.nextElement();
			text.append("   " + key + "=" + (String)parms.get( key )+"\n");
		}
		return new Response(HTTP_OK,MIME_PLAINTEXT,text.toString());
		//return serveFile( uri, header, new File("."), true );
	}


	public static final String
	HTTP_OK = "200 OK",
	HTTP_REDIRECT = "301 Moved Permanently",
	HTTP_FORBIDDEN = "403 Forbidden",
	HTTP_NOTFOUND = "404 Not Found",
	HTTP_BADREQUEST = "400 Bad Request",
	HTTP_INTERNALERROR = "500 Internal Server Error",
	HTTP_NOTIMPLEMENTED = "501 Not Implemented";

	/**
	 * Common mime types for dynamic content
	 */
	public static final String
		MIME_PLAINTEXT = "text/plain",
		MIME_HTML = "text/html",
		MIME_DEFAULT_BINARY = "application/octet-stream";

	// ==================================================
	// Socket & server code
	// ==================================================

	/**
	 * Starts a HTTP server to given port.<p>
	 * Throws an IOException if the socket is already in use
	 */
	public MicroHTTPd( int port ) throws IOException
	{
		serverPort = port;
		
		Enumeration st = new StringVector(
			"htm   text/html "+
			"html  text/html "+
			"txt   text/plain "+
			"asc   text/plain "+
			"gif   image/gif "+
			"jpg   image/jpeg "+
			"jpeg  image/jpeg "+
			"png   image/png "+
			"mp3   audio/mpeg "+
			"m3u   audio/mpeg-url " +
			"pdf   application/pdf "+
			"doc   application/msword "+
			"ogg   application/x-ogg "+
			"zip   application/octet-stream "+
			"exe   application/octet-stream "+
			"class application/octet-stream"," ").elements();
		while ( st.hasMoreElements()){
			theMimeTypes.put((String)st.nextElement(),(String)st.nextElement());
		}
	}
	public void run(){
			try
			{
				String listen = "socket://:" + String.valueOf(serverPort);
				ServerSocketConnection scn = (ServerSocketConnection)Connector.open(listen);
				System.out.println("Started http server on " + scn.getLocalAddress() +":"+ scn.getLocalPort()); 
				while( true ){
					SocketConnection sc = (SocketConnection)scn.acceptAndOpen();
					sc.setSocketOption(SocketConnection.LINGER, 5);
					new Request(this, sc);
				}
			}
			catch ( IOException ioe )
			{
				//TODO
				System.out.println("Exception :");
				ioe.printStackTrace();
			}
	}

	private int serverPort;
	private static Hashtable theMimeTypes = new Hashtable();

	/**
	 * URL-encodes everything between "/"-characters.
	 * Encodes spaces as '%20' instead of '+'.
	 */
	private String encodeUri( String uri )
	{
		String newUri = "";
		Enumeration st = new StringVector( uri, "/ ").elements();
		while ( st.hasMoreElements())
		{
			String tok = (String)st.nextElement();
			if ( tok.equals( "/" ))
				newUri += "/";
			else if ( tok.equals( " " ))
				newUri += "%20";
			else
			{
				//TODO
				//newUri += URLEncoder.encode( tok );
				newUri += tok;
				// For Java 1.4 you'll want to use this instead:
				// try { newUri += URLEncoder.encode( tok, "UTF-8" ); } catch ( UnsupportedEncodingException uee )
			}
		}
		return newUri;
	}

	Hashtable urlmap = new Hashtable();
	
	public void addurl(String url,PageDispatcher page){
		urlmap.put(url, page);	
	}

	public PageDispatcher findPageDispatcher(String url){
		//System.out.println(url);
		return (PageDispatcher)urlmap.get(url);
	}
	public PageDispatcher defaultPageDispatcher(String url){
		//System.out.println(url);
		return null;
	}

}
