package com.pegueroles.microhttpd;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;

public class LineReader extends InputStreamReader {
	int BUFFER_SIZE = 2000;

	public LineReader(InputStream is) {
		super(is);
	}

	private StringBuffer buffer = new StringBuffer();
	
	public String readLine() throws IOException {
		
		String line;
		int length=0,idx1=-1;
		char[] buf;
		char[] buff = new char[BUFFER_SIZE];
		
		idx1 = buffer.toString().indexOf( "\r\n" );
		if ( idx1 >= 0 ) {
			//if found, split into line and rest of tmp
			line = buffer.toString().substring( 0, idx1 );
			buffer = new StringBuffer( buffer.toString().substring( idx1 + 2 ) );
			return line;
		}
		//FPFwhile ( true ) {
			length=0;
			while (ready()){
				buff[length] = (char)read();
				length++;
			}
			// append buffer to temp String
			if ( length < buff.length ) {
				buf  = new char[length];
				System.arraycopy( buff, 0, buf, 0, length );
				buffer.append( new String( buf ) );
			} else {
				buffer.append( new String( buff ) );
			}
			// look in tmp string for \r\n
			idx1 = buffer.toString().indexOf( "\r\n" );
			while ( idx1 >= 0 ) {
				//if found, split into line and rest of tmp
				line = buffer.toString().substring( 0, idx1 );
				// line found
				buffer = new StringBuffer( buffer.toString().substring( idx1 + 2 ) );
				return line;
			}
		//FPF}
		return null;
	}
	
}

