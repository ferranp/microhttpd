package com.pegueroles.microhttpd;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.microedition.io.SocketConnection;

import com.pegueroles.microhttpd.MicroHTTPd;
import com.pegueroles.microhttpd.Response;
import com.pegueroles.microhttpd.LineReader;

public class Request implements Runnable {

	private SocketConnection socket;
	private MicroHTTPd server;
	
	public Request(MicroHTTPd srv , SocketConnection sock )
	{
		socket = sock;
		server = srv;
		Thread t = new Thread( this );
		//t.setDaemon( true );
		t.start();
	}

	public void run()
	{
		try
		{
			
			LineReader is = new LineReader(socket.openInputStream()); 
			if ( is == null) 
				{
					return;
				}
			// Read the request line
			String linex = is.readLine();
			Enumeration st = new StringVector( linex," \t\n\r\f").elements();
			if ( !st.hasMoreElements())
				sendError( Response.HTTP_BADREQUEST, "BAD REQUEST: Syntax error. Usage: GET /example/file.html" );
			String method = (String)st.nextElement();
			if ( !st.hasMoreElements())
				sendError( Response.HTTP_BADREQUEST, "BAD REQUEST: Missing URI. Usage: GET /example/file.html" );

			String uri = (String)st.nextElement();
			// Decode parameters from the URI
			Hashtable parms = new Hashtable();
			int qmi = uri.indexOf( '?' );
			if ( qmi >= 0 )
			{
				decodeParms( uri.substring( qmi+1 ), parms );
				uri = decodePercent( uri.substring( 0, qmi ));
			}
			else uri = decodePercent(uri);

			// If there's another token, it's protocol version,
			// followed by HTTP headers. Ignore version but parse headers.
			// NOTE: this now forces header names uppercase since they are
			// case insensitive and vary by client.
			Hashtable header = new Hashtable();
			if ( st.hasMoreElements())
			{
				String line = is.readLine();
				while ( (line != null) && (line.trim().length() > 0) )
				{
					int p = line.indexOf( ':' );
					header.put( line.substring(0,p).trim().toLowerCase(), line.substring(p+1).trim());
					line = is.readLine();
				}
			}
			// If the method is POST, there may be parameters
			// in data section, too, read it:
			if ( method.equalsIgnoreCase( "POST" ))
			{
				long size = 0x7FFFFFFFFFFFFFFFl;
				String contentLength = (String)header.get("content-length");
				if (contentLength != null)
				{
					try { size = Integer.parseInt(contentLength); }
					catch (NumberFormatException ex) {}
				}
				String postLine = "";
				char buf[] = new char[512];
				int read = is.read(buf);
				while ( read >= 0 && size > 0 && !postLine.endsWith("\r\n") )
				{
					size -= read;
					postLine += String.valueOf(buf, 0, read);
					if ( size > 0 )
						read = is.read(buf);
				}
				postLine = postLine.trim();
				decodeParms( postLine, parms );
			}
			// log request
			System.out.println(dateToString()+" "+ socket.getAddress()+" "+
								" "+method+" "+uri+" ");
			// Ok, now do the serve()
			Response r = server.serve( uri, method, header, parms );
			if ( r == null )
				sendError( Response.HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: Serve() returned a null response." );
			else
				sendResponse( r.status, r.mimeType, r.header, r.data );

			is.close();
		}
		catch ( IOException ioe )
		{
			try
			{
				sendError( Response.HTTP_INTERNALERROR, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
			}
			catch ( Throwable t ) {t.printStackTrace();}
		}
		catch ( InterruptedException ie )
		{
			// Thrown by sendError, ignore and exit the thread.
			//TODO
			ie.printStackTrace();
		}
	}

		/**
		 * Decodes the percent encoding scheme. <br/>
		 * For example: "an+example%20string" -> "an example string"
		 */
		private String decodePercent( String str ) throws InterruptedException
		{
			try
			{
				StringBuffer sb = new StringBuffer();
				for( int i=0; i<str.length(); i++ )
				{
				    char c = str.charAt( i );
				    switch ( c )
					{
				        case '+':
				            sb.append( ' ' );
				            break;
				        case '%':
			                sb.append((char)Integer.parseInt( str.substring(i+1,i+3), 16 ));
				            i += 2;
				            break;
				        default:
				            sb.append( c );
				            break;
				    }
				}
				return new String( sb.toString().getBytes());
			}
			catch( Exception e )
			{
				sendError( Response.HTTP_BADREQUEST, "BAD REQUEST: Bad percent-encoding." );
				return null;
			}
		}

		/**
		 * Decodes parameters in percent-encoded URI-format
		 * ( e.g. "name=Jack%20Daniels&pass=Single%20Malt" ) and
		 * adds them to given Properties.
		 */
		private void decodeParms( String parms, Hashtable p )
			throws InterruptedException
		{
			if ( parms == null )
				return;
			StringVector sv = new StringVector( parms, "&" );
			Enumeration par_enum = sv.elements();
			while ( par_enum.hasMoreElements())
			{
				String e = (String)par_enum.nextElement();
				int sep = e.indexOf( '=' );
				if ( sep >= 0 )
					p.put( decodePercent( e.substring( 0, sep )).trim(),
						   decodePercent( e.substring( sep+1 )));
			}
		}

		/**
		 * Returns an error message as a HTTP response and
		 * throws InterruptedException to stop furhter request processing.
		 */
		private void sendError( String status, String msg ) throws InterruptedException
		{
			System.out.println("sendError");
			sendResponse( status, MicroHTTPd.MIME_PLAINTEXT, null, new ByteArrayInputStream( msg.getBytes()));
			throw new InterruptedException();
		}

		/**
		 * Sends given response to the socket.
		 */
		private void sendResponse( String status, String mime, Hashtable header, InputStream data )
		{
			try
			{
				if ( status == null )
					throw new Error( "sendResponse(): Status can't be null." );

				OutputStream out = socket.openOutputStream();
				PrintStream pw = new PrintStream( out );
				pw.print("HTTP/1.0 " + status + " \r\n");

				if ( mime != null )
					pw.print("Content-Type: " + mime + "\r\n");

				if ( header == null || (String)header.get( "Date" ) == null )
					pw.print( "Date: " + dateToString() + "\r\n");

				if ( header != null )
				{
					Enumeration e = header.keys();
					while ( e.hasMoreElements())
					{
						String key = (String)e.nextElement();
						String value = (String)header.get( key );
						pw.print( key + ": " + value + "\r\n");
					}
				}

				pw.print("\r\n");
				pw.flush();

				if ( data != null )
				{
					byte[] buff = new byte[2048];
					while (true)
					{
						int read = data.read( buff, 0, 2048 );
						if (read <= 0)
							break;
						out.write( buff, 0, read );
					}
				}
				out.flush();
				out.close();
				if ( data != null )
					data.close();
			}
			catch( IOException ioe )
			{
				// Couldn't write? No can do.
				ioe.printStackTrace();
				try { socket.close(); } catch( Throwable t ) {}
			}
		}
	    public static String dateToString() {
	    	Calendar cal = Calendar.getInstance();
	        return ""+ cal.get(Calendar.YEAR)+"-"
	        		 +(cal.get(Calendar.MONTH)+1 )+"-"
	        		 + cal.get(Calendar.DAY_OF_MONTH)+" "+
	        		 hourToString();
	    }
	    
	    public static String hourToString() {
	    	Calendar cal = Calendar.getInstance();
	        int minute = cal.get(Calendar.MINUTE);
	        String min = ""+minute;
	        if (minute <= 9){
	            min = "0"+minute;
	        }
	        return ""+cal.get(Calendar.HOUR_OF_DAY)+":"+min;
	    }    

}
