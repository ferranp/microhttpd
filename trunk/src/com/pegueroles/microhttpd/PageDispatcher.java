package com.pegueroles.microhttpd;

import java.util.Hashtable;

public interface PageDispatcher {
	public Response serve( String uri, String method, Hashtable header, Hashtable parms );

}
